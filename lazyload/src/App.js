import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import './App.css';

function App() {
  const url = 
    "https://www.thecocktaildb.com/api/json/v1/1/search.php?f=a"
  const [images,setImages] = useState([]);

  const getImages = () => {
    Axios.get(url).then((res) => {
      setImages(res.data.drinks);
      console.log(res);
    });
  };

  useEffect(() => {
    getImages();
  },[])

  if(!images){
    return <h1>Loading...</h1>
  }
  return (
    <div className='App'>
      {images.map((image) => {
        return ( 
        <LazyLoadImage
        effect='blur' 
        src={image.strDrinkThumb} 
        alt={image.strDrink} 
        key={image.idDrink}
        height='500px'
        width='400px'
        placeholderSrc={process.env.PUBLIC_URL + '/logo192.png'} 
        />
        );
      })}
    </div>
  );
}
export default App;